# open-city

This web-app is used to provide city and infrastructure data to user.
Following REST API is provided:

### CityController

City is an entity, which basically defines the user's experience, since it should be defined as soon as a user logged in.
Afterwards, user should be given an opportunity to work with streets and houses of the city, as well as with corresponding
crosses and social objects (parks, stations, squares etc.)

Type  | URL | Result
------| ----| ------
GET   |  /city | Current city instance
GET  | /streets | All streets of current city
GET   |  /street/{id} | Street with corresponding id
DELETE  | /street/delete/{id} | Deletes street with id
POST   |  /street/new | Adds new street
PUT  | /street/edit/{id} | Updates existing street

### HouseController

Type  | URL | Result
------| ----| ------
GET   |  /house/{id} | House with id
GET  | /house | All houses of current city
GET  | /house/byStreet/{id} | All houses of street
GET  | /house/byCross/{id} | All houses of cross
DELETE  | /house/{id} | Deletes house with id
POST   |  /house/ | Adds new house
PUT  | /house/{id} | Updates existing house

### SocialObjectController

Type  | URL | Result
------| ----| ------
GET   |  /socialobject/{id} | Social object with id
GET  | /socialobject | All social objects of current city
GET  | /socialobject/byHouse/{id} | All social objects of house
GET  | /socialobject/byCross/{id} | All social objects of cross
GET  | /socialobject/byStreet/{id} | All social objects of street
DELETE  | /socialobject/{id} | Deletes social object with id
POST   |  /socialobject/ | Adds new social object
PUT  | /socialobject/{id} | Updates existing social object

## Swagger UI
1. Deploy an application.
2. Visit localhost:8000/swagger-ui.html

## h2 UI
1. Deploy an application.
2. Visit http://localhost:8000/h2-console
3. Change JDBC URL to "jdbc:h2:mem:testdb" if necessary.
4. Click "Connect".

## Create and run app on Kubernetes

1. `mvn install`
2. Perform `dockerfile:push` to add Docker image to Docker hub.
3. Install Kubernetes (you can choose [Minikube](https://kubernetes.io/docs/setup/minikube/) to deploy locally.
4. Run deploy.sh. It will start minikube locally and deploy here a container based on your Docker image.

Optional: Start Minikube Web UI with `minikube dashboard`

## Start Sonarqube

1. Install [Sonarqube](https://docs.sonarqube.org/latest/setup/get-started-2-minutes) and run locally.
2. Create new project and copy its key to the clipboard.
3. Build project
4. In the project directory run `mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=the-generated-token`
5. Visit [dashboard](http://localhost:9000/dashboard?id={projectname})

## Add caching

Current caching uses Cassandra DB as a fast solution to store most used data closer to the user. 

1. Install Cassandra DB
2. Create a Cassandra keyspace with the name provided in application.properties file (spring.data.cassandra.keyspace-name).
3. Create database using keyspace to provide storage of cached data. 

Use cqlsh-script.cql file to get to know better which databases should be created, or run it directly from cql.