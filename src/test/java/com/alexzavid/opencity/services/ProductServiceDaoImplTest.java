package com.alexzavid.opencity.services;


import com.alexzavid.opencity.config.JpaIntegrationConfig;
import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.cityservices.ICityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaIntegrationConfig.class)
public class ProductServiceDaoImplTest {

    private ICityService iCityService;

    @Autowired
    public void setCityService(ICityService cityService) {
        this.iCityService = cityService;
    }

    @Test
    public void testListMethod() {
        List<Street> streetList = iCityService.getStreetsList();

        assert (!streetList.isEmpty());
    }
}
