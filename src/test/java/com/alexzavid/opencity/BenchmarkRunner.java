package com.alexzavid.opencity;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Warmup;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.SingleShotTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Measurement(batchSize = 10000, iterations = 10)
@Warmup(batchSize = 10000, iterations = 10)
public class BenchmarkRunner {
    public static void main(String[] args) throws Exception {
        org.openjdk.jmh.Main.main(args);
    }

    @Benchmark
    public String benchmarkStringConstructor() {
        return new String("myname");
    }

    @Benchmark
    public String benchmarkStringLiteral() {
        return "myname";
    }

    @Benchmark
    public void arrayListAdd() {
        ArrayList<Object> objects = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            objects.add(i);
        }
    }

    @Benchmark
    public void linkedListAdd() {
        LinkedList<Object> objects = new LinkedList<>();
        for (int i = 0; i < 10000; i++) {
            objects.add(i);
        }
    }
}
