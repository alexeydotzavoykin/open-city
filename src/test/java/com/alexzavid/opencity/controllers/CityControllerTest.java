package com.alexzavid.opencity.controllers;

import com.alexzavid.opencity.controller.CityController;
import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.cityservices.ICityService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CityControllerTest {
    @Mock
    private ICityService cityService;
    @InjectMocks
    private CityController cityController;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(cityController).build();
    }

    @Test
    public void testList() throws Exception{

        List<Street> streets = new ArrayList<>();
        streets.add(new Street());
        streets.add(new Street());

        when(cityService.getStreetsList()).thenReturn((List) streets); //need to strip generics to keep Mockito happy.

        mockMvc.perform(get("/streets/"))
                .andExpect(status().isOk())
                .andExpect(view().name("streets"))
                .andExpect(model().attribute("streets", hasSize(2)));

    }

    @Test
    public void testShow() throws Exception{
        Integer id = 1;

        //Tell Mockito stub to return new product for ID 1
        when(cityService.getStreetById(id)).thenReturn(new Street());

        mockMvc.perform(get("/street/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("street"))
                .andExpect(model().attribute("street", instanceOf(Street.class)));
    }

    @Test
    public void testEdit() throws Exception{
        Integer id = 1;

        when(cityService.getStreetById(id)).thenReturn(new Street());

        mockMvc.perform(get("/street/edit/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("streetform"))
                .andExpect(model().attribute("street", instanceOf(Street.class)));
    }

    @Test
    public void testNewProduct() throws Exception {
        Integer id = 1;

        verifyZeroInteractions(cityService);

        mockMvc.perform(get("/street/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("streetform"))
                .andExpect(model().attribute("street", instanceOf(Street.class)));
    }

    @Test
    @Ignore
    public void testSaveOrUpdate() throws Exception {
        String id = "1";
        String name = "Lenina";
        int length = 12;

        Street returnStreet = new Street();
        returnStreet.setId(Integer.valueOf(id));
        returnStreet.setName(name);
        returnStreet.setLength(length);

        when(cityService.saveOrUpdateStreet(Matchers.<Street>any())).thenReturn(returnStreet);

        mockMvc.perform(post("/street")
                .param("id", "1")
                .param("name", name)
                .param("length", "12"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/street/1"))
                .andExpect(model().attribute("street", instanceOf(Street.class)))
                .andExpect(model().attribute("street", hasProperty("id", is(id))))
                .andExpect(model().attribute("street", hasProperty("length", is(length))))
                .andExpect(model().attribute("street", hasProperty("name", is(name))));

        ArgumentCaptor<Street> returnProduct = ArgumentCaptor.forClass(Street.class);
        verify(cityService).saveOrUpdateStreet(returnProduct.capture());

        assertEquals(id, returnProduct.getValue().getId().toString());
        assertEquals(name, returnProduct.getValue().getName());
        assertEquals(length, returnProduct.getValue().getLength());
    }

    @Test
    @Ignore
    public void testDelete() throws Exception{
        int id = 1;

        mockMvc.perform(get("/street/delete/1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/streets/"));

        verify(cityService, times(1)).deleteStreet(id);
    }
}
