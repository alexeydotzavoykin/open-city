package com.alexzavid.opencity.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

@Configuration
@EnableAutoConfiguration(exclude={CassandraDataAutoConfiguration.class}) // Disabled cache
@ComponentScan("com.alexzavid")
@ActiveProfiles({"jpadao"}) // Now it tests only this profile's behavior
public class JpaIntegrationConfig {
}
