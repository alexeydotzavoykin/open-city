package com.alexzavid.opencity.bootstrap;

import com.alexzavid.opencity.domain.House;
import com.alexzavid.opencity.domain.SocialObject;
import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.cityservices.ICityService;
import com.alexzavid.opencity.services.houseservices.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class SpringJPABooststrap implements ApplicationListener<ContextRefreshedEvent> {

    private ICityService cityService;
    private HouseService houseService;

    @Autowired
    public void setCityService(ICityService cityService) {
        this.cityService = cityService;
    }

    @Autowired
    public void setHouseService(HouseService houseService) {
        this.houseService = houseService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadStreets();
    }

    private void loadStreets() {
        Street street1 = new Street();
        cityService.saveOrUpdateStreet(street1);
        House kirovHouse = new House("13", null, street1, Collections.singletonList(new SocialObject()));
        houseService.createHouse(kirovHouse);
        street1 = new Street(1, "Kirova1", 5, Collections.singletonList(kirovHouse));
        Street street2 = new Street(2, "Oktyabrskaya1", 20, Collections.emptyList());
        cityService.saveOrUpdateStreet(street1);
        cityService.saveOrUpdateStreet(street2);
    }
}
