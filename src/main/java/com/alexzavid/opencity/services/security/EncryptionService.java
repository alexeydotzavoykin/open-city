package com.alexzavid.opencity.services.security;

public interface EncryptionService {
    String encryptString(String input);
    boolean checkPassword(String password, String encryptedPassword);
}
