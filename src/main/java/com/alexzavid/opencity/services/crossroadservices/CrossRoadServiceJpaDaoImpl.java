package com.alexzavid.opencity.services.crossroadservices;

import com.alexzavid.opencity.domain.CrossRoad;
import com.alexzavid.opencity.domain.House;
import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.houseservices.HouseRepository;
import com.alexzavid.opencity.services.security.EncryptionService;
import com.alexzavid.opencity.services.streetservices.StreetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Profile("jpadao")
public class CrossRoadServiceJpaDaoImpl implements CrossRoadService {

    private EntityManagerFactory emf;

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Autowired
    private CrossRoadRepository socialObjectRepository;
    @Autowired
    private HouseRepository houseRepository;
    @Autowired
    private StreetRepository streetRepository;

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<CrossRoad> getCrossRoadsByStreetId(String streetId) {
        Optional<Street> streetById = streetRepository.findById(Integer.valueOf(streetId));
        return streetById.isPresent() ? streetById.get().getCrossList() : Collections.emptyList();
    }

    @Override
    public CrossRoad getCrossRoadByHouseId(String houseId) {
        Optional<House> houseById = houseRepository.findById(Integer.valueOf(houseId));
        // TODO Check availability of cross.
        return houseById.isPresent() ? null : null;
    }

    @Override
    public CrossRoad getCrossRoadById(String id) {
        EntityManager em = emf.createEntityManager();

        return em.find(CrossRoad.class, id);
    }

    @Override
    public CrossRoad createCrossRoad(CrossRoad crossRoad) {
        return this.updateCrossRoad(crossRoad);
    }

    @Override
    public CrossRoad updateCrossRoad(CrossRoad crossRoad) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if(crossRoad.getPassword() != null){
            crossRoad.setEncryptedPassword(encryptionService.encryptString(crossRoad.getPassword()));
        }
        CrossRoad savedCrossRoad = em.merge(crossRoad);
        em.getTransaction().commit();

        return savedCrossRoad;
    }

    @Override
    public void deleteCrossRoad(String id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(House.class, id));
        em.getTransaction().commit();
    }
}
