package com.alexzavid.opencity.services.crossroadservices;

import com.alexzavid.opencity.domain.CrossRoad;

import java.util.List;

public interface CrossRoadService {
    List<CrossRoad> getCrossRoadsByStreetId(String streetId);

    CrossRoad getCrossRoadByHouseId(String crossId);

    CrossRoad getCrossRoadById(String id);

    CrossRoad createCrossRoad(CrossRoad crossRoad);

    CrossRoad updateCrossRoad(CrossRoad crossRoad);

    void deleteCrossRoad(String id);
}
