package com.alexzavid.opencity.services.crossroadservices;

import com.alexzavid.opencity.domain.CrossRoad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrossRoadRepository extends JpaRepository<CrossRoad, Integer> {
}
