package com.alexzavid.opencity.services;

import com.alexzavid.opencity.services.cityservices.ICityService;
import com.alexzavid.opencity.services.cityservices.YaroslavlCityService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Класс для определения конфигурации и бинов приложения.
 */
@Configuration
public class CityConfig {
    @Bean
    @Profile("dev")

    /**
     * Бин доступный только в dev env.
     */
    public ICityService cityService() {
        return new YaroslavlCityService();
    }
}
