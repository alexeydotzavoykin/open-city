package com.alexzavid.opencity.services.houseservices;

import com.alexzavid.opencity.domain.House;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseRepository extends JpaRepository<House, Integer> {
}
