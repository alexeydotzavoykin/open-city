package com.alexzavid.opencity.services.houseservices;

import com.alexzavid.opencity.domain.House;

import java.util.List;

public interface HouseService {
    List<House> getHousesByStreetId(String streetId);

    List<House> getHousesByCrossId(String crossId);

    House getHouseById(int id);

    House createHouse(House house);

    House updateHouse(House house);

    void deleteHouse(int id);
}
