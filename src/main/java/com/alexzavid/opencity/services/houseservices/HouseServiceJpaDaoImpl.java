package com.alexzavid.opencity.services.houseservices;

import com.alexzavid.opencity.domain.House;
import com.alexzavid.opencity.services.security.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.Collections;
import java.util.List;

@Service
@Profile("jpadao")
@Primary
public class HouseServiceJpaDaoImpl implements HouseService {
    private EntityManagerFactory emf;

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<House> getHousesByStreetId(String streetId) {
        // TODO Implement using new repositories
        return Collections.emptyList();
    }

    @Override
    public List<House> getHousesByCrossId(String crossId) {
        return Collections.emptyList();
    }

    @Override
    public House getHouseById(int id) {
        EntityManager em = emf.createEntityManager();

        return em.find(House.class, id);
    }

    @Override
    public House createHouse(House house) {
        return this.updateHouse(house);
    }

    @Override
    public House updateHouse(House house) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if(house.getPassword() != null){
            house.setEncryptedPassword(encryptionService.encryptString(house.getPassword()));
        }
        House savedHouse = em.merge(house);
        em.getTransaction().commit();

        return savedHouse;
    }

    @Override
    public void deleteHouse(int id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(House.class, id));
        em.getTransaction().commit();
    }
}
