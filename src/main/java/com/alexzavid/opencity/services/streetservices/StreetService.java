package com.alexzavid.opencity.services.streetservices;

import com.alexzavid.opencity.domain.Street;

import java.util.List;

public interface StreetService {
    List<Street> getStreetsByCrossId(String crossId);

    Street getStreetById(String id);

    Street createStreet(Street street);

    Street updateStreet(Street street);

    void deleteStreet(String id);
}
