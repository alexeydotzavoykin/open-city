package com.alexzavid.opencity.services.streetservices;

import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.security.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class StreetServiceJpaDaoImpl implements StreetService {
    private EntityManagerFactory emf;

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<Street> getStreetsByCrossId(String crossId) {
        return null;
    }

    @Override
    public Street getStreetById(String id) {
        EntityManager em = emf.createEntityManager();

        return em.find(Street.class, id);
    }

    @Override
    public Street createStreet(Street street) {
        return this.updateStreet(street);
    }

    @Override
    public Street updateStreet(Street street) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if(street.getPassword() != null){
            street.setEncryptedPassword(encryptionService.encryptString(street.getPassword()));
        }
        Street savedStreet = em.merge(street);
        em.getTransaction().commit();

        return savedStreet;
    }

    @Override
    public void deleteStreet(String id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Street.class, id));
        em.getTransaction().commit();
    }
}
