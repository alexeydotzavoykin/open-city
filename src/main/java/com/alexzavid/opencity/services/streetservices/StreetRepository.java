package com.alexzavid.opencity.services.streetservices;

import com.alexzavid.opencity.domain.Street;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StreetRepository extends JpaRepository<Street, Integer> {
}
