package com.alexzavid.opencity.services.socialobjectservices;

import com.alexzavid.opencity.domain.SocialObject;

import java.util.List;

public interface SocialObjectService {
    List<SocialObject> getSocialObjects();
    SocialObject getSocialObjectById(String id);
    List<SocialObject> getSocialObjectsByHouse(String houseId);
    List<SocialObject> getSocialObjectsByCross(String crossId);
    List<SocialObject> getSocialObjectsByStreet(String streetId);
    void deleteSocialObject(String id);
    SocialObject addSocialObject(SocialObject socialObject);
    SocialObject updateSocialObject(SocialObject socialObject);
}
