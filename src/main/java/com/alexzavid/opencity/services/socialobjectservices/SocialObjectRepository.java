package com.alexzavid.opencity.services.socialobjectservices;

import com.alexzavid.opencity.domain.SocialObject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocialObjectRepository extends JpaRepository<SocialObject, Integer> {
}
