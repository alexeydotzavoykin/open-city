package com.alexzavid.opencity.services.socialobjectservices;

import com.alexzavid.opencity.domain.House;
import com.alexzavid.opencity.domain.SocialObject;
import com.alexzavid.opencity.services.houseservices.HouseRepository;
import com.alexzavid.opencity.services.security.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Profile("jpadao")
public class SocialObjectServiceJpaDaoImpl implements SocialObjectService {
    private EntityManagerFactory emf;

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Autowired
    private SocialObjectRepository socialObjectRepository;
    @Autowired
    private HouseRepository houseRepository;

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<SocialObject> getSocialObjects() {
        return Collections.emptyList();
    }

    @Override
    public SocialObject getSocialObjectById(String id) {
        EntityManager em = emf.createEntityManager();

        return em.find(SocialObject.class, id);
    }

    @Override
    public List<SocialObject> getSocialObjectsByHouse(String houseId) {
        Optional<House> houseById = houseRepository.findById(Integer.valueOf(houseId));
        return houseById.isPresent() ? houseById.get().getSocialObjects() : Collections.emptyList();
    }

    @Override
    public List<SocialObject> getSocialObjectsByCross(String crossId) {
        //TODO Implement using new repositories
        return Collections.emptyList();
    }

    @Override
    public List<SocialObject> getSocialObjectsByStreet(String streetId) {
        return Collections.emptyList();
    }

    @Override
    public void deleteSocialObject(String id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(House.class, id));
        em.getTransaction().commit();
    }

    @Override
    public SocialObject addSocialObject(SocialObject socialObject) {
        return this.updateSocialObject(socialObject);
    }

    @Override
    public SocialObject updateSocialObject(SocialObject socialObject) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if(socialObject.getPassword() != null){
            socialObject.setEncryptedPassword(encryptionService.encryptString(socialObject.getPassword()));
        }
        SocialObject savedSocialObject = em.merge(socialObject);
        em.getTransaction().commit();

        return savedSocialObject;
    }
}
