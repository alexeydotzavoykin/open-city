package com.alexzavid.opencity.services.cityservices;

import com.alexzavid.opencity.domain.Street;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class YaroslavlCityService implements ICityService {
    private List<Street> streetsList;

    public YaroslavlCityService() {
        streetsList = Arrays.asList(new Street(1, "Kirova", 5, Collections.emptyList()), new Street(2, "Oktyabrskaya", 20, Collections.emptyList()));
    }

    @Override
    public String getCityName() {
        return "Yaroslavl";
    }

    @Override
    public int getCityPopulation() {
        return 500000;
    }

    @Override
    public List<Street> getStreetsList() {
        return streetsList;
    }

    @Override
    public Street getStreetById(Integer id) {
        return this.getStreetsList().stream().filter(street -> street.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public void deleteStreet(Integer id) {
        streetsList = streetsList.stream().filter(street -> !street.getId().equals(id)).collect(Collectors.toList());
    }

    @Override
    public Street saveOrUpdateStreet(Street street) {
        street.setId(this.getNextId());
        streetsList.add(street);
        return street;
    }

    private int getNextId() {
        return streetsList.isEmpty() ? 1 : streetsList.get(streetsList.size() - 1).getId() + 1;
    }
}
