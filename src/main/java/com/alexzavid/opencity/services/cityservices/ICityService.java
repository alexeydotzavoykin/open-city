package com.alexzavid.opencity.services.cityservices;

import com.alexzavid.opencity.domain.Street;

import java.util.List;

public interface ICityService {
    String getCityName();

    int getCityPopulation();

    List<Street> getStreetsList();

    Street getStreetById(Integer id);

    void deleteStreet(Integer id);

    Street saveOrUpdateStreet(Street street);
}
