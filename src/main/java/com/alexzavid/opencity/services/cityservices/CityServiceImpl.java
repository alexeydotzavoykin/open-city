package com.alexzavid.opencity.services.cityservices;

import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.AbstractMapService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Service
@Profile("map")
public class CityServiceImpl extends AbstractMapService implements ICityService {

    @Override
    public String getCityName() {
        return "Moscow";
    }

    @Override
    public int getCityPopulation() {
        return 14000000;
    }

    @Override
    public List<Street> getStreetsList() {
        return getStreetsList();
    }

    @Override
    public Street getStreetById(Integer id) {
        return (Street) super.getById(id);
    }

    @Override
    public void deleteStreet(Integer id) {
        super.delete(id);
    }

    @Override
    public Street saveOrUpdateStreet(Street domainObject) {
        return (Street) super.saveOrUpdate(domainObject);
    }

    @Override
    protected void loadDomainObjects() {
        domainMap = new HashMap<>();
        Street street1 = new Street(1, "Kirova", 5, Collections.emptyList());
        Street street2 = new Street(2, "Oktyabrskaya", 20, Collections.emptyList());
        domainMap.put(1, street1);
        domainMap.put(2, street2);
    }
}
