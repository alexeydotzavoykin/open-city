package com.alexzavid.opencity.services.cityservices;

import com.alexzavid.opencity.domain.Street;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class RybinskCityService implements ICityService {
    @Override
    public String getCityName() {
        return "Rybinsk";
    }

    @Override
    public int getCityPopulation() {
        return 400000;
    }

    @Override
    public List<Street> getStreetsList() {
        return Arrays.asList(new Street(1, "Ryba", 10, Collections.emptyList()),
                new Street(2, "Lenina", 15, Collections.emptyList()));
    }

    @Override
    public Street getStreetById(Integer id) {
        return this.getStreetsList().stream().filter(street -> street.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public void deleteStreet(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Street saveOrUpdateStreet(Street street) {
        return null;
    }
}
