package com.alexzavid.opencity.services.cityservices;

import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.security.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
@Primary
public class CityServiceJpaDaoImpl implements ICityService {
    private EntityManagerFactory emf;

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public String getCityName() {
        return null;
    }

    @Override
    public int getCityPopulation() {
        return 0;
    }

    @Override
    public List<Street> getStreetsList() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("from Street", Street.class).getResultList();
    }

    @Override
    public Street getStreetById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(Street.class, id);
    }

    @Override
    public void deleteStreet(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Street.class, id));
        em.getTransaction().commit();
    }

    @Override
    public Street saveOrUpdateStreet(Street street) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if(street.getPassword() != null){
            street.setEncryptedPassword(encryptionService.encryptString(street.getPassword()));
        }
        Street savedStreet = em.merge(street);
        em.getTransaction().commit();

        return savedStreet;
    }
}
