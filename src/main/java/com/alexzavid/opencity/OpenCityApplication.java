package com.alexzavid.opencity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude={CassandraDataAutoConfiguration.class}) // Disabled cache, remove and disable MemCacheRepositoryImpl to access caching capabilities
public class OpenCityApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenCityApplication.class, args);
	}

}
