package com.alexzavid.opencity.bl;

import com.alexzavid.opencity.domain.House;
import com.alexzavid.opencity.domain.MemCacheRepository;
import com.alexzavid.opencity.domain.MemCacheWrite;
import com.alexzavid.opencity.services.houseservices.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
public class HouseBlProvider {
    @Autowired
    private HouseService houseService;
    @Autowired
    private MemCacheRepository memCacheRepository;

    private Logger logger = Logger.getLogger(this.getClass().getName());
    // Defines whether correct data for byStreet operation COULD be get from cache
    private boolean byStreetTrust;

    public House getHouse(Integer id) {
        List<MemCacheWrite> houseMemCache = memCacheRepository.findByTitle("getHouse:" + id);
        if (houseMemCache != null && !houseMemCache.isEmpty()) {
            logger.log(Level.FINE, "Got house from memcache");
            return new House(houseMemCache.get(0));
        }
        House houseById = houseService.getHouseById(id);
        memCacheRepository.save(new MemCacheWrite("getHouse:" + id, houseById.toString()));
        return houseById;
    }

    public List<House> getHousesByStreet(Integer id) {
        List<MemCacheWrite> houseMemCache = memCacheRepository.findByTitle("byStreet:" + id);
        if (houseMemCache != null && !houseMemCache.isEmpty()) {
            logger.log(Level.FINE, "Got byStreet from memcache");
            return houseMemCache.stream().map(House::new).collect(Collectors.toList());
        } else {
            List<House> housesByStreetId = houseService.getHousesByStreetId(id.toString());
            memCacheRepository.save(new MemCacheWrite("getHousesByStreet:" + id, housesByStreetId.toString()));
            return housesByStreetId;
        }
    }

    public List<House> getHousesByCross(Integer id) {
        List<House> housesByCrossId = houseService.getHousesByCrossId(id.toString());
        memCacheRepository.save(new MemCacheWrite("getHousesByCross:" + id, housesByCrossId.toString()));
        return housesByCrossId;
    }

    public void deleteHouse(Integer id) {
        // updates getHoustById, getHoustByCross and byStreet caches
        houseService.deleteHouse(id);
        memCacheRepository.removeByTitle("getHouse" + id);
    }

    public House addHouse(House house) {
        House houseInst = houseService.createHouse(house);
        memCacheRepository.save(new MemCacheWrite("addHouse", houseInst.toString()));
        logger.log(Level.FINE, "Created house with id: {}", houseInst.getId());
        // Updates getHouseByCross, getHouseByStreet caches
        return houseInst;
    }

    public House editHouse(House house) {
        House houseInst = houseService.updateHouse(house);
        memCacheRepository.save(new MemCacheWrite("editHouse", houseInst.toString()));
        // Also updates getHouseByCross, getHouseByStreet chaches
        return houseInst;
    }
}
