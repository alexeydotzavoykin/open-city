package com.alexzavid.opencity.controller;

import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.cityservices.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CityController {
    @Autowired
    private ICityService cityService;

    @Value("${spring.application.name}")
    private String name;

    private String attributeName = "street";

    public CityController(ICityService iCityService) {
        this.cityService = iCityService;
    }

    public CityController() {

    }

    @GetMapping(value = "/city")
    public String cityName() {
        return cityService.getCityName() + " " + name;
    }

    @GetMapping(value = "/population")
    public String cityPopulation() {
        return String.valueOf(cityService.getCityPopulation());
    }

    @GetMapping(value = "/streets")
    public String listStreets(Model model) {
        model.addAttribute("streets", cityService.getStreetsList());
        return "streets";
    }

    @GetMapping(value = "/street/{id}")
    public String listStreet(@PathVariable Integer id, Model model) {
        model.addAttribute(attributeName, cityService.getStreetById(id));
        return attributeName;
    }

    @GetMapping(value = "/street/edit/{id}")
    public String listEdit(@PathVariable Integer id, Model model) {
        model.addAttribute(attributeName, cityService.getStreetById(id));
        return "streetform";
    }

    @GetMapping(value = "/street/new")
    public String listCreate(Model model) {
        model.addAttribute(attributeName, new Street());
        return "streetform";
    }

    @PostMapping(value = "/street")
    public String saveOrUpdateStreet(Street street) {
        Street savedStreet = cityService.saveOrUpdateStreet(street);
        return "redirect:/street/" + savedStreet.getId();
    }

    @DeleteMapping(value = "/street/delete/{id}")
    public String deleteStreet(@PathVariable Integer id) {
        cityService.deleteStreet(id);
        return "redirect:/streets/";
    }
}

