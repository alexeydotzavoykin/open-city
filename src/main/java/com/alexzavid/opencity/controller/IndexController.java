package com.alexzavid.opencity.controller;

import com.alexzavid.opencity.services.cityservices.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @Autowired
    private ICityService cityService;
    
    @GetMapping(value = "/index")
    public String index() {
        return "index";
    }
}
