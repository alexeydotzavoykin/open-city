package com.alexzavid.opencity.controller.rest;

import com.alexzavid.opencity.domain.MemCacheRepository;
import com.alexzavid.opencity.domain.MemCacheWrite;
import com.datastax.driver.core.utils.UUIDs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class MemCacheController {
    @Autowired
    private MemCacheRepository memCacheRepository;

    @GetMapping("/MemCacheWrites")
    public ResponseEntity<List<MemCacheWrite>> getAllWrites(@RequestParam(required = false) String title) {
        try {
            List<MemCacheWrite> tutorials = new ArrayList<>();

            if (title == null)
                tutorials.addAll(memCacheRepository.findAll());
            else
                tutorials.addAll(memCacheRepository.findByTitle(title));

            if (tutorials.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/MemCacheWrites/{id}")
    public ResponseEntity<MemCacheWrite> getMemCacheWriteById(@PathVariable("id") String id) {
        Optional<MemCacheWrite> memCacheWrite = memCacheRepository.findById(UUID.fromString(id));

        return memCacheWrite
                .map(cacheWrite -> new ResponseEntity<>(cacheWrite, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/MemCacheWrites")
    public ResponseEntity<MemCacheWrite> createMemCacheWrite(@RequestBody MemCacheWrite memCacheWriteRq) {
        try {
            MemCacheWrite memCacheWrite = memCacheRepository.save(new MemCacheWrite(UUIDs.timeBased(), memCacheWriteRq.getTitle(), memCacheWriteRq.getDescription()));
            return new ResponseEntity<>(memCacheWrite, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("/MemCacheWrites/{id}")
    public ResponseEntity<MemCacheWrite> updateMemCacheWrite(@PathVariable("id") String id, @RequestBody MemCacheWrite memCacheWrite) {
        Optional<MemCacheWrite> tutorialData = memCacheRepository.findById(UUID.fromString(id));

        if (tutorialData.isPresent()) {
            MemCacheWrite memCacheFromCache = tutorialData.get();
            memCacheFromCache.setTitle(memCacheFromCache.getTitle());
            memCacheFromCache.setDescription(memCacheFromCache.getDescription());
            return new ResponseEntity<>(memCacheRepository.save(memCacheFromCache), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/MemCacheWrites/{id}")
    public ResponseEntity<HttpStatus> deleteMemCacheWrite(@PathVariable("id") String id) {
        try {
            memCacheRepository.deleteById(UUID.fromString(id));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @DeleteMapping("/MemCacheWrites")
    public ResponseEntity<HttpStatus> deleteAllMemCacheWrites() {
        try {
            memCacheRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

}

