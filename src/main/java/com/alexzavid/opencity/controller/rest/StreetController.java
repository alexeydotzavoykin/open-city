package com.alexzavid.opencity.controller.rest;

import com.alexzavid.opencity.domain.Street;
import com.alexzavid.opencity.services.streetservices.StreetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/streets")
public class StreetController {
    @Autowired
    private StreetService streetService;

    public StreetController(StreetService streetService) {
        this.streetService = streetService;
    }

    public StreetController() {
    }

    /**
     * Finds an object with specified id.
     * @param id number of street to find.
     * @return Social object with id.
     */
    @GetMapping(value = "/{id}")
    public Street getStreet(@PathVariable Integer id) {
        return streetService.getStreetById(id.toString());
    }

    /**
     * Find streets on cross specified.
     * @param id id of cross to look for streets.
     * @return list of streets on cross.
     */
    @GetMapping(value = "/byCross/{id}")
    public List<Street> getStreetsByCross(@PathVariable Integer id) {
        return streetService.getStreetsByCrossId(id.toString());
    }

    /**
     * Deletes street with specified id.
     * @param id id of streets to delete.
     */
    @DeleteMapping(value = "/{id}")
    public void deleteStreet(@PathVariable Integer id) {
        streetService.deleteStreet(id.toString());
    }

    /**
     * Adds new street.
     * @param street new street.
     * @return created street.
     */
    @PostMapping(value = "/")
    public Street addStreet(Street street) {
        return streetService.createStreet(street);
    }

    /**
     * Edits street.
     * @param street street to edit.
     * @return Updated streets.
     */
    @PutMapping(value = "/{id}")
    public Street editStreet(Street street) {
        return streetService.updateStreet(street);
    }
}
