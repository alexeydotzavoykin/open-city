package com.alexzavid.opencity.controller.rest;

import com.alexzavid.opencity.domain.SocialObject;
import com.alexzavid.opencity.services.socialobjectservices.SocialObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/socialobject")
public class SocialObjectController {
    @Autowired
    private SocialObjectService socialObjectService;

    public SocialObjectController(SocialObjectService socialObjectService) {
        this.socialObjectService = socialObjectService;
    }

    public SocialObjectController() {
    }

    /**
     * Finds an object with specified id.
     * @param id number of social object to find.
     * @return Social object with id.
     */
    @GetMapping(value = "/{id}")
    public SocialObject getSocialObject(@PathVariable Integer id) {
        return socialObjectService.getSocialObjectById(id.toString());
    }

    /**
     * Finds list of social objects available for current city.
     * @return list of social objects.
     */
    @GetMapping(value = "/")
    public List<SocialObject> getSocialObjects() {
        return socialObjectService.getSocialObjects();
    }

    /**
     * Find social objects on street specified.
     * @param id id of street to look for social objects.
     * @return list of social objects on street.
     */
    @GetMapping(value = "/byStreet/{id}")
    public List<SocialObject> getSocialObjectsByStreet(@PathVariable Integer id) {
        return socialObjectService.getSocialObjectsByStreet(id.toString());
    }

    /**
     * Find social objects on cross specified.
     * @param id id of cross to look for social objects.
     * @return list of social objects on cross.
     */
    @GetMapping(value = "/byCross/{id}")
    public List<SocialObject> getSocialObjectsByCross(@PathVariable Integer id) {
        return socialObjectService.getSocialObjectsByCross(id.toString());
    }

    /**
     * Find social objects on house specified.
     * @param id id of house to look for social objects.
     * @return list of social objects on house.
     */
    @GetMapping(value = "/byHouse/{id}")
    public List<SocialObject> getSocialObjectsByHouse(@PathVariable Integer id) {
        return socialObjectService.getSocialObjectsByHouse(id.toString());
    }

    /**
     * Deletes social object with specified id.
     * @param id id of social objects to delete.
     */
    @DeleteMapping(value = "/{id}")
    public void deleteSocialObject(@PathVariable Integer id) {
        socialObjectService.deleteSocialObject(id.toString());
    }

    /**
     * Adds new social object.
     * @param socialObject new social object.
     * @return created social object.
     */
    @PostMapping(value = "/")
    public SocialObject addSocialObject(SocialObject socialObject) {
        return socialObjectService.addSocialObject(socialObject);
    }

    /**
     * Edits social object.
     * @param socialObject social object to edit.
     * @return Updated social objects.
     */
    @PutMapping(value = "/{id}")
    public SocialObject editSocialObject(SocialObject socialObject) {
        return socialObjectService.updateSocialObject(socialObject);
    }
}
