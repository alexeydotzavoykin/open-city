package com.alexzavid.opencity.controller.rest;

import com.alexzavid.opencity.domain.CrossRoad;
import com.alexzavid.opencity.services.crossroadservices.CrossRoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Provides data about crossroads.
 */
@RestController
@RequestMapping("/crossroad")
public class CrossRoadController {
    @Autowired
    private CrossRoadService crossroadService;

    public CrossRoadController(CrossRoadService crossroadService) {
        this.crossroadService = crossroadService;
    }

    public CrossRoadController() {
    }

    /**
     * Gets crossroad by id.
     * @param id id number of crossroad.
     * @return crossroad with specified id.
     */
    @GetMapping(value = "/{id}")
    public CrossRoad getCrossRoad(@PathVariable Integer id) {
        return crossroadService.getCrossRoadById(id.toString());
    }

    /**
     * Gets list of crossroads for street of id.
     * @param id id of street where we are looking for crossroads.
     * @return list of crossroads of street.
     */
    @GetMapping(value = "/byStreet/{id}")
    public List<CrossRoad> getCrossRoadsByStreet(@PathVariable Integer id) {
        return crossroadService.getCrossRoadsByStreetId(id.toString());
    }

    /**
     * List of crossroads for crossroad.
     * @param id id of crossroad.
     * @return List of crossroads on crossroad.
     */
    @GetMapping(value = "/byHouse/{id}")
    public CrossRoad getCrossRoadsByHouse(@PathVariable Integer id) {
        return crossroadService.getCrossRoadByHouseId(String.valueOf(id));
    }

    /**
     * Deletes crossroad with id.
     * @param id id of crossroad to delete.
     */
    @DeleteMapping(value = "/{id}")
    public void deleteCrossRoad(@PathVariable Integer id) {
        crossroadService.deleteCrossRoad(id.toString());
    }

    /**
     * Adds a crossroad to the data storage.
     * @param crossroad crossroad to add.
     * @return created crossroad.
     */
    @PostMapping(value = "/")
    public CrossRoad addCrossRoad(CrossRoad crossroad) {
        return crossroadService.createCrossRoad(crossroad);
    }

    /**
     * Changes created crossroad.
     * @param crossroad edited crossroad.
     * @return new version of crossroad.
     */
    @PutMapping(value = "/")
    public CrossRoad editCrossRoad(CrossRoad crossroad) {
        return crossroadService.updateCrossRoad(crossroad);
    }
}
