package com.alexzavid.opencity.controller.rest;

import com.alexzavid.opencity.bl.HouseBlProvider;
import com.alexzavid.opencity.domain.House;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Provides data about houses.
 */
@RestController
@RequestMapping("/house")
public class HouseController {
    @Autowired
    private HouseBlProvider houseBlProvider;

    public HouseController(HouseBlProvider houseBlProvider) {
        this.houseBlProvider = houseBlProvider;
    }

    public HouseController() {
    }

    /**
     * Gets house by id.
     * @param id id number of house.
     * @return house with specified id.
     */
    @GetMapping(value = "/{id}")
    public House getHouse(@PathVariable Integer id) {
        return houseBlProvider.getHouse(id);
    }

    /**
     * Gets list of houses for street of id.
     * @param id id of street where we are looking for houses.
     * @return list of houses of street.
     */
    @GetMapping(value = "/byStreet/{id}")
    public List<House> getHousesByStreet(@PathVariable Integer id) {
        return houseBlProvider.getHousesByStreet(id);
    }

    /**
     * List of houses for crossroad.
     * @param id id of crossroad.
     * @return List of houses on crossroad.
     */
    @GetMapping(value = "/byCross/{id}")
    public List<House> getHousesByCross(@PathVariable Integer id) {
        return houseBlProvider.getHousesByCross(id);
    }

    /**
     * Deletes house with id.
     * @param id id of house to delete.
     */
    @DeleteMapping(value = "/{id}")
    public void deleteHouse(@PathVariable Integer id) {
        houseBlProvider.deleteHouse(id);
    }

    /**
     * Adds a house to the data storage.
     * @param house house to add.
     * @return created house.
     */
    @PostMapping(value = "/")
    public House addHouse(House house) {
        return houseBlProvider.addHouse(house);
    }

    /**
     * Changes created house.
     * @param house edited house.
     * @return new version of house.
     */
    @PutMapping(value = "/")
    public House editHouse(House house) {
        return houseBlProvider.editHouse(house);
    }
}