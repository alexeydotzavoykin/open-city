package com.alexzavid.opencity.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class CrossRoad extends AbstractDomainClass {
    private String name;

    public CrossRoad() {
    }

    public CrossRoad(String name) {
        this.name = name;
    }

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Street> streetList;

    public List<Street> getStreetList() {
        return streetList;
    }

    public void setStreetList(List<Street> streetList) {
        this.streetList = streetList;
    }


    @Transient
    private String password;
    private String encryptedPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
