package com.alexzavid.opencity.domain;

import com.datastax.driver.core.utils.UUIDs;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class MemCacheWrite {
    @PrimaryKey
    private UUID id;

    private String title;
    private String description;

    public MemCacheWrite() {
    }

    public MemCacheWrite(UUID id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public MemCacheWrite( String title, String description) {
        this.id = UUIDs.timeBased();
        this.title = title;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
