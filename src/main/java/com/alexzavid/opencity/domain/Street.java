package com.alexzavid.opencity.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Street implements DomainObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private int length;

    @Version
    private Integer version;

    @Transient
    private String password;
    private String encryptedPassword;

    @ManyToMany
    private List<CrossRoad> crossList;

    public List<CrossRoad> getCrossList() {
        return crossList;
    }

    public void setCrossList(List<CrossRoad> crossList) {
        this.crossList = crossList;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<House> houses;

    public List<House> getHouses() {
        return houses;
    }

    public void setHouses(List<House> houses) {
        this.houses = houses;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Street() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Street(int id, String name, int length, List<House> houses) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.houses = houses;
    }

    public Street(int id, String name, int length, List<House> houses, List<CrossRoad> crossRoadList) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.houses = houses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
