package com.alexzavid.opencity.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class House extends AbstractDomainClass {
    private String fullAdressName;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private HouseAdmin admin;

    @ManyToOne(cascade = {CascadeType.ALL, CascadeType.PERSIST})
    private Street street;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.PERSIST})
    private List<SocialObject> socialObjects;

    @Transient
    private String password;
    private String encryptedPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public House() {
    }

    public House(String fullAdressName, HouseAdmin admin, Street street, List<SocialObject> socialObjects) {
        this.fullAdressName = fullAdressName;
        this.admin = admin;
        this.street = street;
        this.socialObjects = socialObjects;
    }

    public House(MemCacheWrite memCacheWrite) {
        this.fullAdressName = memCacheWrite.getDescription();
    }

    public String getFullAdressName() {
        return fullAdressName;
    }

    public void setFullAdressName(String fullAdressName) {
        this.fullAdressName = fullAdressName;
    }

    public HouseAdmin getAdmin() {
        return admin;
    }

    public void setAdmin(HouseAdmin admin) {
        this.admin = admin;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public List<SocialObject> getSocialObjects() {
        return socialObjects;
    }

    public void setSocialObjects(List<SocialObject> socialObjects) {
        this.socialObjects = socialObjects;
    }
}
