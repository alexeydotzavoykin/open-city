package com.alexzavid.opencity.domain;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.List;
import java.util.UUID;

public interface MemCacheRepository extends CassandraRepository<MemCacheWrite, UUID> {
    @AllowFiltering
    List<MemCacheWrite> findByTitle(String title);
    @AllowFiltering
    List<MemCacheWrite> removeByTitle(String title);
}
