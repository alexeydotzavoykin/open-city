package com.alexzavid.opencity.domain;

import com.datastax.driver.core.CloseFuture;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.RegularStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Class for stubbing Cassandra DB when no caching needed
 */
@Component
public class MemCacheRepositoryImpl implements MemCacheRepository {
    @Bean
    CassandraTemplate cassandraTemplate() {
        return new CassandraTemplate(new Session() {
            @Override
            public String getLoggedKeyspace() {
                return null;
            }

            @Override
            public Session init() {
                return null;
            }

            @Override
            public ListenableFuture<Session> initAsync() {
                return null;
            }

            @Override
            public ResultSet execute(String s) {
                return null;
            }

            @Override
            public ResultSet execute(String s, Object... objects) {
                return null;
            }

            @Override
            public ResultSet execute(String s, Map<String, Object> map) {
                return null;
            }

            @Override
            public ResultSet execute(Statement statement) {
                return null;
            }

            @Override
            public ResultSetFuture executeAsync(String s) {
                return null;
            }

            @Override
            public ResultSetFuture executeAsync(String s, Object... objects) {
                return null;
            }

            @Override
            public ResultSetFuture executeAsync(String s, Map<String, Object> map) {
                return null;
            }

            @Override
            public ResultSetFuture executeAsync(Statement statement) {
                return null;
            }

            @Override
            public PreparedStatement prepare(String s) {
                return null;
            }

            @Override
            public PreparedStatement prepare(RegularStatement regularStatement) {
                return null;
            }

            @Override
            public ListenableFuture<PreparedStatement> prepareAsync(String s) {
                return null;
            }

            @Override
            public ListenableFuture<PreparedStatement> prepareAsync(RegularStatement regularStatement) {
                return null;
            }

            @Override
            public CloseFuture closeAsync() {
                return null;
            }

            @Override
            public void close() {

            }

            @Override
            public boolean isClosed() {
                return false;
            }

            @Override
            public Cluster getCluster() {
                return null;
            }

            @Override
            public State getState() {
                return null;
            }
        });
    }

    @Override
    public List<MemCacheWrite> findByTitle(String title) {
        return null;
    }

    @Override
    public List<MemCacheWrite> removeByTitle(String title) {
        return null;
    }

    @Override
    public <S extends MemCacheWrite> S save(S s) {
        return null;
    }

    @Override
    public <S extends MemCacheWrite> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<MemCacheWrite> findById(UUID uuid) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(UUID uuid) {
        return false;
    }

    @Override
    public List<MemCacheWrite> findAll() {
        return null;
    }

    @Override
    public List<MemCacheWrite> findAllById(Iterable<UUID> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(UUID uuid) {

    }

    @Override
    public void delete(MemCacheWrite memCacheWrite) {

    }

    @Override
    public void deleteAll(Iterable<? extends MemCacheWrite> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public Slice<MemCacheWrite> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends MemCacheWrite> S insert(S s) {
        return null;
    }

    @Override
    public <S extends MemCacheWrite> List<S> insert(Iterable<S> iterable) {
        return null;
    }
}
